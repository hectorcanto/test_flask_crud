import time
import requests
from colorama import Fore
from pprint import pprint


FILENAME = "technical_test_input.in"
HOST = "0.0.0.0"
PORT = 5000


def extract_features(string):
    external_id, rest = string.split("@")
    external_id = int(external_id[1:].strip())

    position, size = rest.strip().split(":")
    x, y = position.split(",")
    x = int(x)
    y = int(y)

    width, height = size.strip().split("x")
    width = int(width)
    height = int(height)
    return external_id, x, y, width, height


def _color_wrap(string, color=Fore.GREEN):
    return color + string + Fore.RESET


def main():
    session = requests.Session()  # To reuse the connection
    headers = {"Content-Type": "application/json"}
    session.headers.update(headers)
    with open(FILENAME) as buf:
        for line in buf:
            ext_id, x, y, width, height = extract_features(line)
            body = dict(external_id=ext_id, x=x, y=y, width=width, height=height)
            response = session.post(f"http://{HOST}:{PORT}/claims", json=body)
            if response.status_code != 201:
                print(_color_wrap(str(response.status_code)), response.content)
                # TODO change for logging with stdout output

    response = session.get(f"http://{HOST}:{PORT}/screen/free")
    print(_color_wrap("Claims without overlapping"), response.json())
    pprint(response.json())
    response = session.get(f"http://{HOST}:{PORT}/screen/overlapping")
    print(_color_wrap("Number of spots with overlapping"), response.json())


if __name__ == '__main__':
    start = time.time()
    main()
    duration = time.time() - start
    print(_color_wrap(f"Insert took {round(duration, 2)} seconds", color=Fore.YELLOW))
