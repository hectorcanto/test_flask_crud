FROM python:3.6.5-alpine3.7

ENV PROJECT_DIR /opt/project
ENV SOURCE_DIR $PROJECT_DIR/source

# Adding Python requirements
COPY requirements ./requirements/

# Install requirements
RUN apk add --update-cache --repository http://dl-cdn.alpinelinux.org/alpine/latest-stable/main \
     python3 git musl-dev postgresql-dev gcc  && \
     python3 -m ensurepip && \
     rm -r /usr/lib/python*/ensurepip && \
     pip3 install setuptools && \
     if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
     if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
     rm -r /root/.cache

RUN pip install --upgrade pip
RUN pip install -r requirements/dev.txt

# Prepare environment
RUN mkdir -p $SOURCE_DIR
ENV FLASK_APP=$SOURCE_DIR/launcher.py

# COPY code
COPY source $SOURCE_DIR/

#  Prepare test artfifacts
COPY tests $PROJECT_DIR/tests/

EXPOSE 5000
WORKDIR $PROJECT_DIR
CMD flask run --host 0.0.0.0