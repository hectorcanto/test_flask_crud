import pytest

from inserter import extract_features


@pytest.mark.parametrize("string, expected", [
    ("#2 @ 84,482: 15x11", (2, 84, 482, 15, 11)),
    ("#1345 @ 567,627: 19x15", (1345, 567, 627, 19, 15))
])
def test_extract_features(string, expected):
    assert extract_features(string) == expected
