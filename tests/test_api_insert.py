import pytest
from flask import json

from .factories import claims
from source.database import db
from source.models import Claim


headers = {"Content-Type": "application/json"}
pytestmark = pytest.mark.usefixtures("clear_tables")


def test_insert_claim(api_client):
    body = claims.ClaimDictFactory()
    response = api_client.post("/claims", data=json.dumps(body), headers=headers)

    assert response.status_code == 201, response.data

    response_body = json.loads(response.data)
    assert "_id" in response_body

    for key in body:
        assert body[key] == response_body[key]

    assert db.session.query(Claim).count() == 1


@pytest.mark.parametrize("body", [
    claims.ClaimDictFactory(x=1100),
    claims.ClaimDictFactory(x=-5),
    claims.ClaimDictFactory(height=0),
    claims.ClaimDictFactory(width=1100),
    claims.ClaimDictFactory(x=950, width=100),
])
def test_insert_fails_bad_request(api_client, body):
    response = api_client.post("/claims", data=json.dumps(body), headers=headers)
    assert response.status_code == 400, response.data
    assert db.session.query(Claim).count() == 0


def test_insert_fails_repeated(api_client):
    claims.ClaimFactory2DB(external_id=100),
    body = claims.ClaimDictFactory(external_id=100)
    response = api_client.post("/claims", data=json.dumps(body), headers=headers)
    assert response.status_code == 409, response.data
