import pytest
from flask import json

from .factories import claims
from source.database import db
from source.models import Claim


headers = {"Content-Type": "application/json"}
pytestmark = pytest.mark.usefixtures("clear_tables")


@pytest.mark.parametrize("method,", [
    "head", "get", "post", "put", "delete", "options", "trace"
])
def test_bad_endpoint(api_client, method):
    api_call = getattr(api_client, method)
    response = api_call("/bad_endpoint")
    assert response.status_code == 404, response.data


def test_get_claim_detail(api_client):
    claim = claims.ClaimFactory2DB()
    claim_id = str(claim.external_id)
    response = api_client.get(f"/claims/{claim_id}")

    assert response.status_code == 200, response.data

    response_body = json.loads(response.data)

    for key in response_body:
        if key == "_id":
            assert response_body[key] == str(getattr(claim, key))
        elif "_at" in key:
            assert response_body[key] == getattr(claim, key).timestamp()
        else:
            assert response_body[key] == getattr(claim, key)


def test_detail_not_found(api_client):
    response = api_client.get("/claims/111124")
    assert response.status_code == 404, response.data


def test_detail_bad_id(api_client):
    response = api_client.get("/claims/string_invalid")
    assert response.status_code == 400, response.data


@pytest.mark.parametrize("body", [
    {"x": 200},
    {"y": 200},
    {"height": 200},
    {"width": 200},
])
def test_update_claim(api_client, body):
    existing_claim = claims.ClaimFactory2DB(x=100, y=100, width=100, height=100)

    response = api_client.put(f"/claims/{existing_claim.external_id}",
                              data=json.dumps(body), headers=headers)
    assert response.status_code == 200, response.data
    response_body = json.loads(response.data)

    for k, v in body.items():
        assert response_body[k] == v


@pytest.mark.parametrize("body", [
    {"x": 1100}, {"y": 1100}, {"height": 1100}, {"width": 1100},
    {"x": 950}, {"y": 950}, {"height": 150}, {"width": 150},
])
def test_update_fails(api_client, body):
    existing_claim = claims.ClaimFactory2DB(x=900, y=900, width=90, height=90)

    response = api_client.put(f"/claims/{existing_claim.external_id}",
                              data=json.dumps(body), headers=headers)
    assert response.status_code == 400, response.data


def test_update_not_found(api_client):
    body = {"x": 500},
    response = api_client.put("/claims/12345",
                              data=json.dumps(body), headers=headers)
    assert response.status_code == 404, response.data


def test_update_bad_id(api_client):
    body = {"x": 500},
    response = api_client.put("/claims/not_a_number",
                              data=json.dumps(body), headers=headers)
    assert response.status_code == 400, response.data


def test_get_claim_collection(api_client):
    claims.ClaimFactory2DB()
    claims.ClaimFactory2DB()
    claims.ClaimFactory2DB()
    claims.ClaimFactory2DB()
    claims.ClaimFactory2DB()
    db.session.commit()
    response = api_client.get("/claims")
    assert response.status_code == 200, response.data

    response_body = json.loads(response.data)
    assert len(response_body) == 5
