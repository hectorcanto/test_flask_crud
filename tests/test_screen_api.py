import pytest
from flask import json

from .factories import claims


pytestmark = pytest.mark.usefixtures("clear_tables")

FREE_VIEW = "/screen/free"
COUNT_VIEW = "/screen/overlapping"


def test_screen_only_one(api_client):
    claim = claims.ClaimFactory2DB()
    response = api_client.get(FREE_VIEW)
    assert response.status_code == 200, response.data
    response_body = json.loads(response.data)
    assert len(response_body) == 1
    assert response_body[0]["_id"] == str(claim._id)


def test_overalapping(api_client):
    claims.ClaimFactory2DB(x=100, y=100, width=10, height=10)
    claims.ClaimFactory2DB(x=109, y=109)
    response = api_client.get(FREE_VIEW)
    assert response.status_code == 200, response.data
    response_body = json.loads(response.data)
    assert len(response_body) == 0


def test_almost_overlapping(api_client):
    claims.ClaimFactory2DB(x=200, y=200, width=10, height=10)
    claims.ClaimFactory2DB(x=210, y=210)
    response = api_client.get(FREE_VIEW)
    assert response.status_code == 200, response.data
    response_body = json.loads(response.data)
    assert len(response_body) == 2


def test_one_free_two_overlap(api_client):
    claims.ClaimFactory2DB(x=200, y=200, width=10, height=10)
    claims.ClaimFactory2DB(x=200, y=200, width=10, height=10)
    claims.ClaimFactory2DB(x=500, y=500, width=10, height=10)
    response = api_client.get(FREE_VIEW)
    assert response.status_code == 200, response.data
    response_body = json.loads(response.data)
    assert len(response_body) == 1


def test_three_overlap(api_client):
    claims.ClaimFactory2DB(x=200, y=200, width=10, height=10)
    claims.ClaimFactory2DB(x=215, y=215, width=10, height=10)
    claims.ClaimFactory2DB(x=209, y=209, width=10, height=10)
    response = api_client.get(FREE_VIEW)
    assert response.status_code == 200, response.data
    response_body = json.loads(response.data)
    assert len(response_body) == 0


def test_overlap_count(api_client):
    claims.ClaimFactory2DB(x=200, y=200, width=10, height=10)
    claims.ClaimFactory2DB(x=200, y=200, width=10, height=10)

    response = api_client.get(COUNT_VIEW)
    assert response.status_code == 200, response.data
    response_body = json.loads(response.data)
    assert response_body == 100
