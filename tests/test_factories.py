from .factories import claims
from source.models import Claim
from source.database import db


def test_dict_factory():
    c = claims.ClaimDictFactory()
    assert set(c.keys()) == {"external_id", "x", "y", "width", "height"}
    assert c["x"] < 1000
    assert c["y"] < 1000
    assert c["width"] < 1000
    assert c["height"] < 1000


def test_fixed_dict_factory():
    c = claims.ClaimDictFactory(x=5, y=5, width=5, height=5)
    assert c["x"] == 5
    assert c["y"] == 5
    assert c["width"] == 5
    assert c["height"] == 5


def test_regular_factory(current_app, clear_tables):
    c = claims.ClaimFactory(x=5, y=5, width=5, height=5)
    assert c.x == 5
    assert c.y == 5
    assert c.width == 5
    assert c.height == 5

    with current_app.app_context():
        db.session.add(c)
        db.session.commit()
        assert db.session.query(Claim).count() == 1


def test_db_factory(current_app, clear_tables):
    claims.ClaimFactory2DB()
    claims.ClaimFactory2DB()
    claims.ClaimFactory2DB()  # Careful, they are not committed
    assert db.session.query(Claim).count() == 3
