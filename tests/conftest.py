import pytest


from source import main, database
from source.config import Testing
from .factories.claims import assign_session_to_factories


def _clear_tables(db):
    meta = db.metadata

    for table in reversed(meta.sorted_tables):
        db.session.execute(table.delete())
    db.session.commit()
    db.session.close()


def _close_db(db):
    db.drop_all()
    db.session.close()
    db.engine.dispose()


@pytest.fixture(scope="session", autouse=True)
def current_app():
    app = main.create_app(Testing)
    db = database.db
    with app.app_context():
        # TODO CREATE TABLE IF NOT EXISTS
        _clear_tables(db)
        assign_session_to_factories(db.session)
        yield app

        _clear_tables(db)
        _close_db(db)


@pytest.fixture
def api_client(current_app):
    return current_app.test_client()


@pytest.fixture(scope="function")
def clear_tables(current_app):
    yield
    with current_app.app_context():
        _clear_tables(database.db)
