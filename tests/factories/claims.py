from uuid import uuid4
from functools import partial

from factory import Factory, alchemy, LazyFunction as LF
from factory.fuzzy import FuzzyChoice as Choice
from source.models import Claim


class AbstractClaimFactory(Factory):
    """Claim Base factory, all other derive from it"""
    class Meta:
        model = Claim
        abstract = True
    _id = LF(uuid4)
    external_id = Choice(range(1, 1000))
    x = Choice(range(0,500))
    y = Choice(range(0,500))
    width = Choice(range(0, 500))
    height = Choice(range(0, 500))


class ClaimFactory(AbstractClaimFactory):
    """Regular Factory, to add to the session"""
    class Meta:
        exclude = ("_id",)


class ClaimFactory2DB(AbstractClaimFactory, alchemy.SQLAlchemyModelFactory):
    """
    Elements created with this factory are inserted automatically into the DB, if
    the session is assigned.
    """
    class Meta:
        # sqlalchemy_session = create_db_session
        sqlalchemy_session_persistence = "commit"


def assign_session_to_factories(session):
    """Makes the DB factory able to write."""
    ClaimFactory2DB._meta.sqlalchemy_session = session


def dict_factory(factory, **kwargs):
    """Creates a dict factory, to use in API tests."""
    return factory.stub(**kwargs).__dict__


ClaimDictFactory = partial(dict_factory, ClaimFactory)