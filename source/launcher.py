from pprint import pprint

from .main import create_app
from source.models.claims import Claim
from source.database import db
from source.validation import claim_schema

app = create_app()


@app.shell_context_processor
def make_shell_context():
    return dict(app=app, db=db, Claim=Claim, pprint=pprint, claim_schema=claim_schema)


@app.teardown_appcontext
def shutdown_session(exception=None):
    db.session.remove()
