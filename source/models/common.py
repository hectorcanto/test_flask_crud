from datetime import datetime, timezone

from sqlalchemy import Column as _
from sqlalchemy.types import TIMESTAMP
from sqlalchemy.dialects.postgresql import UUID

from source.database import db


UUID_TYPE = UUID(as_uuid=True)
dt_type = TIMESTAMP(timezone=True)


def utc_now():
    return datetime.now(timezone.utc)


class CommonColumns(db.Model):  # TODO maybe replace with app.db
    __abstract__ = True
    created_at = _(dt_type, default=utc_now)
    updated_at = _(dt_type, default=utc_now, onupdate=utc_now)
