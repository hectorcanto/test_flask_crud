from uuid import uuid4
from sqlalchemy import Column as _, Integer

from .common import CommonColumns, UUID_TYPE


class Claim(CommonColumns):
    __tablename__ = "claims"

    _id = _(UUID_TYPE, primary_key=True, default=uuid4)
    external_id = _(Integer, nullable=False, unique=True)
    x = _(Integer, nullable=False)  # From the top-left edge
    y = _(Integer, nullable=False)
    width = _(Integer, nullable=False)
    height = _(Integer, nullable=False)

    @property
    def end_x(self):
        return self.x + self.width

    @property
    def end_y(self):
        return self.y + self.height

    @property
    def size(self):
        return self.width, self.height

    @property
    def dict(self):
        result = dict(self.__dict__)  # deep copy
        del result["_sa_instance_state"]
        return result
