from uuid import UUID
from datetime import datetime

import sqlalchemy.orm.state
from flask import Flask
from flask.json import JSONEncoder

from . import database, config
from.views import ClaimView, non_overlapping, num_overlapping, catch_all


def add_endpoints(app):
    # TODO user Flask-Restful and transform Claim views into a Resource Class
    # or DRF Class View directly from ORM
    app.add_url_rule('/claims', 'claim_collection',
                     view_func=ClaimView.claim_collection, methods=["GET"])
    app.add_url_rule('/claims/<id>', 'claim_detail',
                     view_func=ClaimView.claim_detail, methods=["GET"])
    app.add_url_rule('/claims', 'insert_claim',
                     view_func=ClaimView.insert_claim, methods=["POST", "HEAD"])
    app.add_url_rule('/claims/<id>', 'claim_update',
                     view_func=ClaimView.update_claim, methods=["PUT"])
    app.add_url_rule('/screen/free', 'screen_free_claims',
                     view_func=non_overlapping, methods=["GET"])
    app.add_url_rule('/screen/overlapping', 'screen_overlap_count',
                     view_func=num_overlapping, methods=["GET"])
    app.add_url_rule('/<path:path>', 'custom_404', view_func=catch_all,
                     methods=["HEAD", "GET", "POST", "PUT", "DELETE", "OPTIONS", "TRACE"])


class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            return str(obj)
        if isinstance(obj, datetime):
            return obj.timestamp()
        if isinstance(obj, sqlalchemy.orm.state.InstanceState):
            return None
        return JSONEncoder.default(self, obj)


def create_app(configuration=config.Config):
    app = Flask("screenclaim_app")
    app.json_encoder = CustomJSONEncoder
    app.config.from_object(configuration)
    with app.app_context():
        database.db.init_app(app)
        database.db.create_all()
    add_endpoints(app)
    return app
