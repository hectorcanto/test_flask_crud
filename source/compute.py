import numpy as np


def compute_matrix(claims):
    base = np.zeros((1000, 1000), dtype=int)
    for c in claims:
        base[c.x:c.end_x, c.y:c.end_y] += 1
    return base


def find_non_overlapping(matrix, claims):
    non_overlapping = []
    for c in claims:
        if (matrix[c.x:c.end_x, c.y:c.end_y] == np.ones(c.size, dtype=int)).all():
            non_overlapping.append(c.dict)
    return non_overlapping


def count_conflicting_inches(matrix):
    return len(matrix[np.where( matrix > 1 )])