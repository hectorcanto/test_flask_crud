
from flask import jsonify, request, abort, make_response

from .models import Claim
from .database import db
from .validation import claim_schema, claim_schema_external
from .compute import compute_matrix, find_non_overlapping, count_conflicting_inches

def catch_all(*args, **kwargs):
    """
    This endpoint return a customized 404 of any non-matching call, to avoid
    misguiding 405.
    """
    abort(make_response(jsonify(
        message="This is not the endpoint you're looking for."), 404))


def cool_abort(code, message):
    abort(make_response(jsonify(
        message=message), code))


def non_overlapping():
    # NOTE When many claims the non_overlapping computation might be slow
    claim_list = db.session.query(Claim).all()
    base = compute_matrix(claim_list)
    non_overlapping = find_non_overlapping(base, claim_list)
    return jsonify(non_overlapping)


def num_overlapping():
    # NOTE When many claims the non_overlapping computation might be slow
    claim_list = db.session.query(Claim).all()
    base = compute_matrix(claim_list)
    overlapping_count = count_conflicting_inches(base)
    return jsonify(overlapping_count)


def _run_validation(body, session):
    errors = claim_schema.validate(body)
    if errors:
        session.rollback()
        cool_abort(400, errors)
    errors = claim_schema.validates_occupation(body)
    if errors:
        session.rollback()
        cool_abort(400, errors)


def _validate_ext_id(id):
    errors = claim_schema_external.validate({"external_id": id})
    if errors:
        cool_abort(400, errors)


class ClaimView:
    @staticmethod
    def claim_detail(id):
        _validate_ext_id(id)
        result = db.session.query(Claim).filter_by(external_id=id).all()
        if not result:
            abort(404, "Claim not found")
        result = result[0]
        return jsonify(result.dict)

    @staticmethod
    def claim_collection():
        claim_list = db.session.query(Claim).all()
        claim_list = [claim.dict for claim in claim_list]
        return jsonify(claim_list)

    @staticmethod
    def delete_claim(id):
        _validate_ext_id(id)
        result = db.session.query(Claim).filter_by(external_id=id).all()
        if not result:
            cool_abort(404, "Claim not found.")
        result = result[0]
        db.session.delete(result)
        db.session.commit()
        return None, 202

    @staticmethod
    def update_claim(id):
        _validate_ext_id(id)
        result = db.session.query(Claim).filter_by(external_id=id).first()
        if not result:
            cool_abort(404, "Claim not found.")

        body = request.json
        for key in body:
            setattr(result, key, body[key])
        _run_validation(result.dict, db.session)

        db.session.add(result)
        db.session.commit()
        result._id  # Forces to get the fields for transient object
        return jsonify(result.dict)

    @staticmethod
    def insert_claim():
        body = request.json
        _run_validation(body, db.session)

        result = db.session.query(Claim).filter_by(external_id=body["external_id"]).first()
        if result:
            cool_abort(409, "A Claim with the same external ID already exists.")

        new_claim = Claim(**body)
        db.session.add(new_claim)
        db.session.commit()
        new_claim._id  # Forces to get the fields for transient object
        return jsonify(new_claim.dict), 201


# TODO update claim
# TODO delete claim
