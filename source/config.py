
def get_db_uri(db_name):
    return f"postgresql+psycopg2://postgres:postgres@0.0.0.0:5432/{db_name}"


class Config(object):
    FLASK_DEBUG = True
    SQLALCHEMY_DATABASE_URI = get_db_uri("screenclaim")
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class Testing(object):
    FLASK_DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = get_db_uri("screenclaim")  # TODO use a test_db_name
    SQLALCHEMY_TRACK_MODIFICATIONS = False
