from marshmallow import Schema, fields, validates_schema, ValidationError
from marshmallow.validate import Range


X_AXIS_ERROR = "Claim exceeds X boundary."
Y_AXIS_ERROR = "Claim exceeds Y boundary."


class BaseClaimSchema(Schema):
    _id = fields.UUID(required=False)
    external_id = fields.Int(required=True)
    x = fields.Int(required=True, validate=[Range(0, 999)])
    y = fields.Int(required=True, validate=[Range(0, 999)])
    width = fields.Int(required=True, validate=[Range(1, 1000)])
    height = fields.Int(required=True, validate=[Range(1, 1000)])


class ClaimSchema(BaseClaimSchema):

    @validates_schema(skip_on_field_errors=True)
    def validates_occupation(self, data):
        errors = list()
        if data["x"] + data["width"] > 1000:
            errors.extend([X_AXIS_ERROR])

        if data["y"] + data["height"] > 1000:
            errors.extend([Y_AXIS_ERROR])

        if errors:
            raise ValidationError(" ".join(errors))
        return errors
