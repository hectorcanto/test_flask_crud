from .claims import ClaimSchema, BaseClaimSchema
claim_schema = ClaimSchema()
claim_schema_external = BaseClaimSchema(only=("external_id", ))
