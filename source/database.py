from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def truncate_db():
    connection = db.engine.connect()
    connection.execute(db.metadata.sorted_tables[0].delete())


def create_if_not_exist(db_name="screenclaim"):
    connection = db.engine.connect()
    statement = f"CREATE DATABASE {db_name}"
    connection.execute(statement)
