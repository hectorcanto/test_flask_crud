# SOC Crysis

This project has been developed and tested using python 3.6.7 over Ubuntu 16.04.5.


## Installation

1. Checkout DEPENDENCIES and install them with your favourite package manager, _apt_ or _yum_ f.i.


2. Create a python virtual environment

    ```
    virtualenv -p python3.6 evn
    source env/bin/activate
    ```

3. Once inside the virtual env. install pip dependencies

```pip install -r requirements/dev.txt```

4. Create the database schema for the solution
```
docker-compose -f docker-compose.test.yml up -d
docker exec postgres psql -U postgres -c 'CREATE DATABASE screenclaim'
```


### Recommendations

Allow the local user to use docker:
```bash
sudo groupadd docker
sudo usermod -aG docker $USER
```

Depending on the distribution some dependencies might not be installable,
for example _numpy_.

## Implementation

### API 

Since it is a simple CRUD API and I'm more used to it lately, I implemented
this solution with Flask and SQLAlchemy, among other technologies.

Another approach would be using Django Rest Framework, using a Class View for the _Claim_ Resource
or adding Flask-Restful to the existing solution.


### Database

As it is my preferred SQL DB, I've chosen PostgresSQL.


### Logic: Screen representation 

Regarding the overlapping computation I realized that the claims could be represented by a matrix,
the content of the matrix being the number of claims in a spot (inch).

If a spot has a 0, it is free of claims. If it has a 1, only 1 claim. And so on. So, if a claim
area has ones (if the sub-matrix is all ones) then the claim can be fulfilled.

For simplicity I computed the screen every time I made a call, but for speed the matrix could
be stored in an in-memory cache, update it on each claim and persist it in a DB or a file (for
instance a pickled Python object or using _numpy.save_)


### Validation

Validation is made with Marshmallow, but it could have been made with Pydantic or other
libraries. In case of using DRF, we could have used its Serializers or Django-rest-marshmallow
like in https://audiolion.github.io/django/2018/01/31/marshmallow-drf.html.

In my opinion, it is best to use the best tools for the job, specially those which really stands
out. Also, relying on a big framework might lock you to the technology and make your project
too coupled to the framework.


## How to run 

The recommended way to run this solution is docker-compose, however in case of an error use
the local guide. If the error persists, contact the author.


### Locally
1. Create a virtual environment and activate it:

```
virtualenv -p python3.6 env
source env/bin/activate
```

2. Enter it and install requirements

```pip install -r requirements/dev.txt```

3. Insert the claims from the provided input 
Open two terminals
In the first one launch the API

```
export FLASK_APP=source/launcher.py
flask run
```

In the other terminal launch a script that will post all the claims

```python inserter.py```



4. To get the non-overlapping claim(s) and the overlapping count call
the endpoints _/screen/free_ _/screen/overlapping_ with this script:

```python result.py```


### Run with docker-compose

1. First build the image

```docker build -t hcanto-ibits:1 .```

or simply ```make build```


2. Second, launch the containers, with docker-compose
```docker-compose up -d```

3. Launch the Claim inserter and check the result. 
```
python inserter.py 
python result.py
```

### Run with docker

If you want to run the API on demand use docker alongside docker-compose.

1. First build the image


```docker build -t hcanto-ibits:1 .```

or simply ```make build```.

2. Second, launch the DB, with docker-compose

```docker-compose -f docker-compose.test.yml up -d```

3. Launch the API container

```make run```

4. Launch the Claim inserter and check the result:

```
python inserter.py 
python result.py
```

## Testing
To test you will need a live DB. For that, launch the database container:

```docker-compose -f docker-compose.test.yml up -d```

And simply use ```pytest``` from the project folder and it will run the entire suite.

NOTE that the test are pointing to the same database as the API, so any previous data
will be deleted running the test suite. It is pending to point the test to an auxiliary
database schema.


## Know issues

- When launched with ```docker run``` the API cannot be gracefully stopped with Control+C. This
issue could be solved be using Gunicorn.

- Tests point to the same DB schema than the regular application. This causes claims to be deleted
after any most test ar run, either the whole suite or those that access the database.

- Marshmallow could be used with its sqlalchemy binding, this might have simplified the validation
aspect of the solution.

- In a real-life work, the proper way to continue this application is to use a Rest-friendly framework
like Django Rest Framework or Flask-Restful.


## Bibliography

http://flask.pocoo.org/docs/1.0/
https://www.sqlalchemy.org/
https://marshmallow.readthedocs.io/en/2.x-line/
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world
https://docs.scipy.org/doc/numpy-1.15.0/reference/
https://direnv.net/
https://docs.pytest.org/en/latest/
https://stackoverflow.com
http://docs.python-requests.org/en/master/user/advanced/
https://gist.github.com/hectorcanto/40a7ecbd9e02b0550840f850165bc162
