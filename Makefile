DOCKER_NAME=hcanto-ibits:1
DOCKER_LOCALHOST=$(shell /sbin/ifconfig docker0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $$1}')

build:
	@docker build -t $(DOCKER_NAME) .

setup:
	@docker-compose up -d

run:
	echo $(DOCKER_LOCALHOST)
	echo $(DOCKER_NAME)
	docker run -p 5000:5000 --add-host postgres:$(DOCKER_LOCALHOST) --rm $(DOCKER_NAME)

